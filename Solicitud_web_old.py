
import obspy
from obspy.core.util import NamedTemporaryFile
from obspy.clients.fdsn import Client as FDSN_Client
from obspy.clients.iris import Client as OldIris_Client
#from ScrolledText import *
from peticion_web import *
from OpenFile_multiple import *
import decimal
import random
import math
from Guardar_datos import *
#from tkFileDialog import askopenfilename
from Lectura_file import *
from Rutas import *
from Pos_Analisis_Resultados import *
from ReName import *
import os
from shutil import rmtree
from scross import *
from Dia_juliano import *

class Solicitud_web:

	def trabajar(self):
		


		self.abrirArchivoStation()
		self.abrirArchivoFile()
		inicia_peticiones=peticion_web()
		inicio_dia_j=Dia_juliano()
		inicia_xcross=scross()
		



		self.year_an = str(self.year)
		self.Channel='HHZ'
		self.day=str(self.day)
		self.month=str(self.month)
		
		if len(self.day)<2:
			self.day='0'+str(self.day)
		else:
			self.day=str(self.day)

		if len(self.month)<2:
			self.month='0'+str(self.month)
		else:
			self.month=str(self.month)



		## se incia la configuración nueva

		day_j=inicio_dia_j.main(self.year_an, self.month,self.day)
		print ("dia julino", day_j)
		day_j=str(day_j)
		while len(day_j)<3:
			day_j='0'+day_j
		print ('==============================================')
		#print(dias_j)
		dias_analisis=[]

		

		#============================ Se indica path para guardar resultados ===============================
		inicia=Guardar_datos()
		
		#ruta_guardar_datos_end=inicia.guardar_directorio(Tipo)
		ax_pat=os.getcwd()
		ruta_guardar_datos_end=ax_pat+"/"+self.code
		print (ruta_guardar_datos_end)



		if not os.path.exists(ruta_guardar_datos_end):
			os.makedirs(ruta_guardar_datos_end)




		ruta_guardar_datos=ruta_guardar_datos_end+"/results_pre"
		print ('==============================================')
		print ( " path para guardar datos", ruta_guardar_datos)
		#datos_rep[4]=ruta_guardar_datos
		estaciones=self.estaciones
		self.ruta_guardar_rep_datos=ruta_guardar_datos+'/tmp'
		if not os.path.exists(self.ruta_guardar_rep_datos):
			os.makedirs(self.ruta_guardar_rep_datos)
		


		
		for sta in range(len(self.estaciones)):
			#sta=3
			#self.pathArchivoDE=[]
			aux_rep=[]
			print (self.year_an, self.day, self.Channel, self.estaciones[sta], self.month, self.code)
			datos_rep=[self.month, self.day,self.code, self.estaciones[sta],""]
			st_z=inicia_peticiones.main( self.year_an, self.month, self.day, self.code, self.estaciones[sta], self.Channel)
			st_e=inicia_peticiones.main( self.year_an, self.month, self.day, self.code, self.estaciones[sta], 'HHE')
			st_n=inicia_peticiones.main( self.year_an, self.month, self.day, self.code, self.estaciones[sta], 'HHN')
			#aux_rep.append(st_z)
			name_rep_z=self.ruta_guardar_rep_datos+'/'+self.code+'.'+self.estaciones[sta]+'..HHZ.M.'+str(self.year_an)+'.'+str(day_j)+'.000000.SAC'
			name_rep_e=self.ruta_guardar_rep_datos+'/'+self.code+'.'+self.estaciones[sta]+'..HHE.M.'+str(self.year_an)+'.'+str(day_j)+'.000000.SAC'
			name_rep_n=self.ruta_guardar_rep_datos+'/'+self.code+'.'+self.estaciones[sta]+'..HHN.M.'+str(self.year_an)+'.'+str(day_j)+'.000000.SAC'
			#print ("name_rep", name_rep)
			#aux_rep.append(name_rep)
			if st_z!= None and st_e != None and st_n != None:
				st_z.write(name_rep_z, format='SAC')
				st_e.write(name_rep_e, format='SAC') 
				st_n.write(name_rep_n, format='SAC')
		
		
		if not os.path.exists(ruta_guardar_datos):
			os.makedirs(ruta_guardar_datos)
		
		#self.pathArchivoDE.append(st)
		
		#***********************
		self.abrirArchivo()		
		respuesta=self.validar()		
		if respuesta == True:
			for i in range(len(self.pathArchivoDE)):

				for ii in estaciones:
					if self.pathArchivoDE[i].find(ii) !=-1:
						self.estacion_1=ii
						#print("estacion1",self.estacion_1)



				#=========================================
				for j in range(len(self.pathArchivoDF)):

					for jj in estaciones:
						#print("DF",self.pathArchivoDF[j])
						if self.pathArchivoDF[j].find(jj) != -1:
							self.estacion_2=jj
							#print("estacion2",self.estacion_2)
					if self.estacion_1==self.estacion_2:
						dia_aux=inicia_xcross.main( self.pathArchivoDE[i], self.pathArchivoDF[j], ruta_guardar_datos, estaciones, self.year_an)
						#verifica los dias analizados por el sistema y lo guarda en el vector dias analisis
						if dia_aux not in dias_analisis:
							if dia_aux!= None:
								dias_analisis.append(dia_aux)

			

		print ("--- ", dias_analisis, "----")
		inicia_pos_analisis=Pos_Analisis_Resultados()
		#print "++", ruta_guardar_datos_end, "++", d
		for d in dias_analisis:
			print ("++", ruta_guardar_datos_end, "++", d)
			inicia_pos_analisis.main(ruta_guardar_datos_end,ruta_guardar_datos, d, estaciones, self.ruta_repository, self.year_an)
		#inicia=ClasificaPrueba()
		#inicia.main(self.pathArchivoDE, self.pathArchivoDF)
		inicia_rename=ReName()
		inicia_rename.main(self.year_an, estaciones, ruta_guardar_datos, ruta_guardar_datos_end, dias_analisis)
		#rmtree(ruta_guardar_datos_end)

		

			
			



	def validar(self):
		valido = True
		##print  'entra'

		

		return valido
	def abrirArchivo(self):
		#print ':)****'
		file=self.ruta_guardar_rep_datos
		#print (file)
		archivos=os.listdir(file)
		archivos=sorted(archivos)
		#print (archivos)
		directorios=[]
		path=[]
		aux_return=[]
		for i in range(len(archivos)):
			archivos[i]= file+'/'+archivos[i]

			#print(i,':  ', archivos[i])
			#if os.path.isdir(archivos[i])==True:
				#temp=os.listdir(archivos[i])
				#temp=sorted(temp)
				#print ("temp", temp)
				#for j in range(len(temp)):
			if archivos[i].find('HHZ')!=-1:
				path.append(archivos[i])
		aux_return=[path, file]
		self.ruta_repository=aux_return[1]
		#print (path, "++++")
		archivo =aux_return[0]
		
		self.pathArchivoDE=archivo
		print  (self.pathArchivoDE)

	def pathfileDF(self):
		

		self.filename = self.pathCat
		return(self.filename)
	def abrirArchivoFile(self):

		archivo =self.pathfileDF()


		file=self.pathCat
		#print (file)
		archivos=os.listdir(file)
		archivos=sorted(archivos)
		directorios=[]
		path=[]
		aux_return=[]
		for i in range(len(archivos)):
			archivos[i]= file+'/'+archivos[i]
			if os.path.isdir(archivos[i])==True:
				temp=os.listdir(archivos[i])
				temp=sorted(temp)
				for j in range(len(temp)):
					if temp[j].find('HHZ')!=-1:
						path.append(archivos[i]+'/'+temp[j])
		aux_return=[path, file]
		
		self.pathArchivoDF=aux_return[0]
		print  (self.pathArchivoDF)

		#self.etiquetaPath1.config(text="Select files:"+str(len(archivo)))
		#self.pathArchivoDF=archivo
		#print  self.pathArchivoDF

	def abrirArchivoStation(self):

		
		path_sta=self.path_sta
		inicio=Lectura_file()
		#path="/home/carlos/Dropbox/Implementacion_DOC/examples/stations.txt"
		a=inicio.main(path_sta)
		aux=[]
		for i in range(len(a)):
			if i!=0:
				aux.append(a[i][0])
		self.estaciones=aux
		print (self.estaciones)

		#archivo =self.pathfileDF()
		#self.etiquetaPath2.config(text="Select files:"+str(len(self.estaciones)))
		#self.pathArchivoSta=archivo
		#print  self.pathArchivoDF
	def pathfileDF(self):
		#Tipo=" Select files in format SAC calog"
		#inicia=OpenFile_multiple()

		self.filename = self.pathCat
		return(self.filename)		
	def main(self):
		valor = "" #para el inicio los entry tenga b


		print("Input of data fpr correlation web")
		self.year=input("Input year of analisys (YYYY):  ")
		try:
			self.year=int(self.year)
			if (self.year)>=1954:
				print("year is:  ", self.year)
				status_year=True
		except Exception as e:
			print("format incorrect")
			status_year=False
		
			while status_year == False:
				self.year=input("Input year of analisys (YYYY):  ")
				try:
					self.year=int(self.year)
					if (self.year)>=1954:
						print("year is:  ", self.year)
						status_year=True
				except Exception as e:
					print("format incorrect")
					status_year=False



		self.code=input("Input FDSN code:  ")
		try:
			print("FDSN code is: ", self.code)
			self.code = self.code.upper()
			status_code=True
		except Exception as e:
			print("error input FDSN code")
			status_code=False

		while status_code == False:
			self.code=input("Input FDSN code:  ")
			try:
				print("FDSN code is: ", self.code)
				self.code = self.code.upper()
				status_code=True
			except Exception as e:
				print("error input FDSN code")
				status_code=False
			
			

		self.month=input("Input initial month (mm):  ")
		try:
			self.month=int(self.month)
			if self.month >=1 and self.month<=12:
				print("Initial month is: ", self.month)
				status_month=True
			else:
				print("month incorrect")
				status_month==False
		except Exception as e:
			print("format incorrect")
			status_month=False

		while status_month== False:
			self.month=input("Input initial month:  ")
			try:
				self.month=int(self.month)
				if self.month >=1 and self.month<=12:
					print("Initial month is: ", self.month)
					status_month=True
				else:
					print("month incorrect")
					status_month==False
			except Exception as e:
				print("format incorrect")
				status_month=False



		self.day=input("Input initial day (dd)")
		if not self.year % 4 and (self.year % 100 or  not self.year % 400):
			bisi=True
		else:
			bisi=False
		meses=[31, 28, 31,30, 31, 30, 31, 31, 30, 31, 30, 31]
		month=self.month
		if bisi == True:
			meses[1]=meses[1]+1

		self.day_mont=meses[month-1]
		try:
			self.day=int(self.day)

			
			

			if self.day>=1 or self.day<=self.day_mont:
				status_day=True
		except Exception as e:
			print("Error, format incorrect")
			self.day_mont=meses[month-1]
			status_day=False


		while status_day==False:
			print("day incorrect, please input the day of month: ", self.month, "in the range(1-",self.day_mont,")")
			self.day=input("Input initial day (dd)")
			try:
				self.day=int(self.day)

				if self.day>=1 or self.day<=self.day_mont:
					status_day=True
			except Exception as e:
				print("Error, format incorrect")
				self.day_mont=meses[month-1]
				status_day=False


		
		archivo=input("Input path catalog (masters)")
		

		try:
			if (os.path.isdir(archivo)) :
				self.pathCat=archivo
				status_df=True
		except Exception as e:
			print("Error, format incorrect")
			status_df=False
		while status_df==False:
			print("path not is a dir")
			archivo=input("Input path catalog (masters)")
		

			try:
				if (os.path.isdir("/home/carlos/Documentos/System-cmd")) :
					self.pathCat=archivo
					status_df=True
			except Exception as e:
				print("Error, format incorrect")
				status_df=False
		



		self.path_sta=input("Input path file stations")

		

		try:
			if (os.path.isfile(self.path_sta)) :
				status_sta=True
		except Exception as e:
			print("Error, format incorrect")
			status_sta=False

		while status_sta== False:
			self.path_sta=input("Input path file stations")

		

			try:
				if (os.path.isfile(self.path_sta)) :
					status_sta=True
			except Exception as e:
				print("Error, format incorrect")
				status_sta=False


		self.mostrarInfo()

	def mostrarInfo(self):
		os.system("clear")
		print ("informayion input")
		print("year is:\n ", self.year)
		print("FDSN code is:\n ", self.code)
		print("month is:\n ", self.month)
		print("day is:\n ", self.day)
		print("Path catalog master is:\n ", self.pathCat)
		print("Path file station is:\n ", self.path_sta)

		print("\n *******************************")
		resp=input("right information? (Y/N)")

		if resp == "Y" or resp =="y" or resp== "yes":
			self.trabajar()
		else:
			print("1.-input information")
			print("2.-cancelar")
			choice=input("Select choice (1-2):\n")

			try:
			
				choice=int(choice)
				print ("choice select: ",choice)
				if choice <=0 or choice>=3:
					os.system("clear")
					print("Choice incorrect, please select choice between 1 and 2\n")

			except:
				os.system("clear")
				choice=0
				print("format incorrect, please enter number between 1 and 2\n")


			while choice <=0 or choice>=3:

			
				print("1.-input information")
				print("2.-cancelar")
				choice=input("Select choice (1-2):\n")
				try:
				
					choice=int(choice)
					print ("choice select: ",choice)
					if choice <=0 or choice>=3:
						os.system("clear")
						print("Choice incorrect, please select choice between 1 and 2\n")
				except:
					os.system("clear")
					choice=0
					print("Choice incorrect, please select choice between 1 and 2\n")

			if choice ==1:
				self.main()
			else:
				self.cancelar()
		
	def cancelar(self):
		os.system("clear")
		'''
		self.app = Tk()
		self.app.title('Input of data for correlation')
		self.app.geometry("1000x300")
		self.app.maxsize(800, 300)

		#VP -> VENTANA PRINCIPAL
		vp = Frame(self.app)
		
		vp.grid(column=0, row=0, padx=(70,70), pady=(20,20)) #posicionar los elementos en tipo matriz, padx es para margen
		vp.columnconfigure(0,weight=1)  #tamanio relativo a las columnas
		vp.rowconfigure(0,weight=1)


		#DATOS ENTRADA POR PATH
		
		etiqueta = Label(vp,text="year: ")
		etiqueta.grid(column=0, row=4) 


		self.entrada_texto = Entry(vp,width=20,textvariable=valor) #unidaes relativas
		self.entrada_texto.grid(column=1,row=4)

		#**********************************************************************************************
		#FDSN code
		etiqueta = Label(vp,text="FDSN code")
		etiqueta.grid(column=0, row=5) 
		self.code= Entry(vp,width=20,textvariable=valor) #unidaes relativas
		self.code.grid(column=1,row=5)

		#***********************************************************************************************
		#NUMERO DE NEURONAS
		#etiqueta = Label(vp,text="Numero: ")
		#etiqueta.grid(column=0, row=6) 
		
		#self.NoNeurona = Entry(vp,width=20,textvariable=valor) #unidaes relativas
		#self.NoNeurona.grid(column=1,row=6)



		#NUMERO ITERACIONES
		etiqueta = Label(vp,text="Initial month: ")
		etiqueta.grid(column=0, row=7) 
		
		self.month = Entry(vp,width=20,textvariable=valor) #unidaes relativas
		self.month.grid(column=1,row=7)		#self.entradaIter.config(state=DISABLED)
		#ERROR CUADRATICO MEDIO
		etiqueta = Label(vp,text="Initial day: ")
		etiqueta.grid(column=0, row=8) 
		#
		self.day = Entry(vp,width=20,textvariable=valor) #unidaes relativas
		self.day.grid(column=1,row=8)


		#self.entradaMSE.config(state=DISABLED)

		etiquetaDF = Label(vp,text="Select set data ")
		etiquetaDF.grid(column=0, row=1) #especificar en la final y columna 
	
		boton = Button(vp, width=21, text="Select catalog (masters)", command=self.abrirArchivoFile)
		boton.grid(column=2,row=1)
 	

		etiquetaDF = Label(vp,text="Select set stations ")
		etiquetaDF.grid(column=0, row=0) #especificar en la final y columna 
	
		boton = Button(vp, width=21, text="Select file stations", command=self.abrirArchivoStation)
		boton.grid(column=2,row=0)

		

		

		self.etiquetaPath1 = Label(vp,text=" ")
		self.etiquetaPath1.grid(column=1, row=1) 
		
		self.etiquetaPath2 = Label(vp,text=" ")
		self.etiquetaPath2.grid(column=1, row=0) 


		
		# #EJECUCION
		boton = Button(vp, width=20, text="Run", command=self.trabajar)
		boton.grid(column=1,row=12)


	


		botonSalir = Button(vp, width=20, text="Quit", command=self.app.destroy)
		botonSalir.grid(column=1,row=13)
		


		#CREDITOS
		etiqueta = Label(vp,text="Elaborado por: CARLOS RAMIREZ PINA")
		etiqueta.grid(column=1, row=40) 
		self.app.mainloop()
		'''






