Para ejecutar CrossQuake puede realizarlo desde un contenedor Docker

1.- instalar Docker

    https://www.docker.com/

2.- Iniciar  Docker y ejecutar: 

    docker pull cramzp/crossquake:tag

3.- verificar

    docker images

4.- Ejecutar contenedor de docker con crossquake

    docker run -it cramzp/crossquake:tag bash

5.- Ejecutar el scrip crossquake

    cd crossquake

    pipenv shell

    python Principal.py

6.- Para salir escribir exit+enter

Nota para ejecutar nuevamente el contenedor solo es necesario realizar los siguientes pasos

1.- docker ps -a

2.- docker start xxxxxxxx(CONTAINER ID)

3.- docker exec -it xxxxxxx(CONTAINER ID) bash

4.- cd crossquake

    pipenv shell

    python Principal.py

5.- Para salir escribir exit+enter
